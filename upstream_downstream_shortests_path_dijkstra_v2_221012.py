# -*- coding: utf-8 -*-

"""
Name upstream_downstream_shortests_path_dijkstra_v20.py

Python Version 3.7

@author: Nicolas Cadieux
Copyright (C) <2019> <Nicolas Cadieux>
GPL-3.0-or-later

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version. This program is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.  You should have received a copy of the GNU
General Public License along with this program.
If not, see <https://www.gnu.org/licenses/>.

NEW FEATURES, FIXES AND KNOW ISSUES


*******New: v2.0
            - Checks for 64-bit Python interpreter
            - Calculates distances by river order ('RO')
            - Calculate full matrix instead of only upper half.
              This will take more than twice the time but useful for small runs.
            - New testing mode for single thread testing with verbose output in
                 output file
            - Now can work with a source node and target node file
            - Distance_maxtrix_route() function replaces upper_distance_maxtrix_route()
            - Graph now contains all the variables from the input network file.
             This is will facilitate future development.

*******Fixes v2.0
            - Search and destroy word "lenght" in code
            - Rename output ...connecting_edges_WKT.csv by entry and exit
              cost.
            - Code style cleanup
            - Fix float point and float trim error by changing shapely function
                ShapelyObject.wkt and str(shapely object) to
                .wkt.dumps(pt, trim=False).
            - Normalize output csv files format
            - Replace length = 0 by 0.0
            - Fix ROUTE_RESULT_DCT_MAX_SIZE not working systematically

*******New:v1.1
            - Code cleanup
            - Remove route nodes from outputs as this was not a useful output
            - Refactorization of some variable names for more clarity
            - Change output route text file formats.
            - Cleanup imports (Thanks Yogesh Bhosle for your input.)
            - New user variable (UNIQUE_ID) for unique id field in the source
              and target node file
            - Checks every geometry in file for multi-part geometries.
              Reject file if it's the case.
            - Checks for UNIQUE_ID presence in file field name
            - Can use 3D shapefiles.  Z will be removed from individual
              geometries.
            - Checks if UNIQUE_ID is present and IS UNIQUE!


*******Fixes: v.1.1
            - Fix crash when input source and target file had no 'gridcode'
              field
            - Fix crash when 3D geometries were used.
            - Fix crash when multipoint geometries were used.
            - Fix float point errors

*******Know issues:
            - No out of memory error if 32 bits version of Python is used
            - When reading gpgk files, gdal gives warnings. This is not an error
              and can be safely ignored.

******* #TODO:
            - Build network tolerance error and network error checking
            - New output file format (shp or geopackage) for gis data and Graph
            - Calculate length of entry cost.


# =============================================================================
# *******Help and inspirations:
# https://gis.stackexchange.com/questions/303789/shortest-path-between-one-point-to-every-other-points
# https://gis.stackexchange.com/questions/213369/how-to-calculate-edge-length-in-networkx/213431#213431
# https://networkx.github.io/documentation/stable/reference/algorithms/shortest_paths.html
# https://gis.stackexchange.com/questions/256955/how-to-load-a-weighed-shapefile-in-networkx
# https://github.com/caesar0301/s2g
# https://gis.stackexchange.com/questions/166515/networkx-saving-shortest-path-of-openstreetmap-data-as-an-ogr-friendly-format
# https://networkx.github.io/documentation/stable/release/migration_guide_from_1.x_to_2.0.html
# https://gis.stackexchange.com/questions/321356/how-to-create-a-weighted-square-lattice-as-well-as-a-weighted-network-graph
# https://networkx.github.io/documentation/stable/reference/algorithms/generated/networkx.algorithms.shortest_paths.weighted.dijkstra_path_length.html#networkx.algorithms.shortest_paths.weighted.dijkstra_path_length
# https://stackoverflow.com/questions/30770776/networkx-how-to-create-multidigraph-from-shapefile
# https://github.com/karimbahgat/PyCRS
# https://stackoverflow.com/questions/13784192/creating-an-empty-pandas-dataframe-then-filling-it
# =============================================================================
"""
# =============================================================================
# May be used for trouble shooting.Some cleanup needed.
# import argparse
# import fiona
# from fiona.crs import from_epsg,from_string,to_string
# import matplotlib.pyplot as plt
# import pyproj
# from sys import getsizeof
# from osgeo import gdal
# from osgeo import ogr
# from shapely.ops import *
# from shapely.wkt import *
# import fiona; fiona.supported_drivers
# from shapely.geometry import *
# from copy import *
# from shapely import speedups
# from shapely.ops import polygonize
# from shapely.ops import snap
# =============================================================================

import os
import time
import sys
import multiprocessing
import pickle

from multiprocessing import Pool
from itertools import zip_longest
from collections import defaultdict
from collections import deque
from copy import deepcopy

import pandas as pd
import geopandas as gpd
import networkx as nx
from shapely.geometry import Point
from shapely.geometry import LineString
from shapely.ops import nearest_points
from shapely.ops import split
from shapely.ops import cascaded_union
from shapely.wkt import loads

# New imports
from shapely import wkt
from inspect import currentframe, getframeinfo

START_TIME = time.time()

# =============================================================================
#                 Mandatory User Input Variables
# =============================================================================


# =============================================================================
#                          Output parameters
# =============================================================================

# Add WKT geometry of routes in results. (Will be slower and files much larger)
INCLUDE_WKT_IN_ROUTES = True

# =============================================================================
#                            Input file namess
# =============================================================================

# Full path to the line network file. Does not handle multilines objects.
# Make sure you keep the "r" before the path ex: r"c:\...."

INPUT_NETWORK_FILE_SHP = os.path.abspath(
    r"X:\temp\SampleNetwork\Sample_Network.shp")

# Full path to source and target point file. Does not handle multipoint obj.

INPUT_SOURCE_TARGET_POINTS = os.path.abspath(
    r"X:\temp\SampleNetwork\sample_source_target_nodes.shp")


# Name of length variable in INPUT_NETWORK_FILE_SHP
INFILE_LENGTH = 'Length'
# Name of river order variable in INPUT_NETWORK_FILE_SHP
UNIQUE_ID = 'gridcode'

# =============================================================================
#                             NEW FOR VERSION 2
# Calculate the total distance for each river order going up and down stream.
# Add the posiblility to calculate full matrix. This is more than twice the 
# the time needed to calculate only the upper half matrix.
#
# - RO is ignored if CLASSIFY_BY_RIVER_ORDER is not True
#
# - CALCULATE_UPPER_HALF_MATRIX_ONLY will change to False if 
#   USE_SOURCE_AND_TARGET_FILES is True.  See distance_maxtrix_route()
#
# - INPUT_TARGET_POINTS is ignored if USE_SOURCE_AND_TARGET_FILES = False  
# =============================================================================

TESTING_MODE = False # True or False

CLASSIFY_BY_RIVER_ORDER = True # True or False
RO = 'ros' # Field name for River Order variable in network file.
CALCULATE_UPPER_HALF_MATRIX_ONLY = True
USE_SOURCE_AND_TARGET_FILES = False  # True or False
INPUT_TARGET_POINTS = os.path.abspath(
    r"X:\temp\SampleNetwork\Destinations.shp") # if above is True

# On Linux r'Sample_Network.shp', Just the file name if default path is set in Anaconda

# =============================================================================
#                    Directory setup and files naming
# =============================================================================

BASIN = r'test'  # Your region of study. Will be used in file names.
OUTPUT_FILE_MAIN_DIRECTORY = os.path.abspath(
    r'X:\temp\SampleNetwork\Results_'+BASIN)

# On Linux r'/home/ncadieux/Documents/SampleNetwork/Results_Test/Results_'+BASIN)

# =============================================================================
#                         Perfomance setup
# =============================================================================
# Returns the CPU count. May use an Int instead 10
THREADS = multiprocessing.cpu_count()-2

# N_POINTS = Chunk size for each thread. Watch system memory.
# Each output file will have N_Points results written to it. 10000 is good if
# no wkt is produced.

N_POINTS = 1000000

# Number of route to calculate before thread writes result to files.
# Should be smaller than N_POINTS if N_POINTS is very large (>10000). If
# INCLUDE_WKT_IN_ROUTES = True, then lower this value if you have memory
# problems.

ROUTE_RESULT_DCT_MAX_SIZE = 100000


# =============================================================================
#                  Optional variables
# =============================================================================

# Tolerance for end node snap
# No other tolerance currently buid in.
# Do not mess with for now.
END_NODE_TOLERANCE = 0.01


OUTPUT_GRAPH_SUBDIRECTORY = os.path.join(
    OUTPUT_FILE_MAIN_DIRECTORY, 'Trouble_shooting', 'GIS')

OUTPUT_TIE_OUTSIDE_NODE_SUBDIRECTORY = os.path.join(
    OUTPUT_FILE_MAIN_DIRECTORY, 'Trouble_shooting', 'GIS')

OUTPUT_ROUTE_SUBDIRECTORY = os.path.join(
    OUTPUT_FILE_MAIN_DIRECTORY, 'Routes')

PYTHO_LIST_DICT_VARIABLES_SUBDIRECTORY = os.path.join(
    OUTPUT_FILE_MAIN_DIRECTORY, 'Trouble_shooting', 'Python_variables')

GRAPH_EDGE_FILE_OUTPUT = os.path.join(
    OUTPUT_GRAPH_SUBDIRECTORY, BASIN + '_graph_edges')

GRAPH_NODES_FILE_OUTPUT = os.path.join(
    OUTPUT_GRAPH_SUBDIRECTORY, BASIN + '_graph_nodes')

TESTING_MODE_SUBDIRECTORY = os.path.join(
    OUTPUT_FILE_MAIN_DIRECTORY, 'Trouble_shooting')


# =============================================================================
#                 End of user input variables
# =============================================================================

def create_directory(path):
    """Create a directory. Will pass if directory has been added
      by another tread."""
    # err = ''
    if os.path.isdir(path):
        pass
    else:
        try:
            os.makedirs(path)
        except OSError as err:            
        # except WindowsError as err:  This only works on Windows box
            print (err)
            return err


def code_line_id():
    """
    Usage print ('code line {}'.format(code_line_id()))
    
    Returns: code line number
    -------
    TYPE
        int

    """
    return currentframe().f_back.f_lineno
    
def alpha_sort(lst):
    '''
    Parameters
    ----------
    lst : TYPE
        DESCRIPTION.

    Returns
    -------
    lst : TYPE
        DESCRIPTION.

    '''
    lst = [str(i) for i in lst]
    lst.sort()
    lst = [int(i) if i.isdigit() else i for i in lst ]
    lst = [str(i) for i in lst]
    return lst

def parse_river_order(file_str):
    """
    
    Parameters
    ----------
    file_str : File name str

    Returns
    -------
    Unique river order list with alpha numerical sorting as default dict
    is never ordered. list may contain numbers and/or string but all 
    outputs will be strings.
    """    
    gpd_df = gpd.read_file(file_str)
    
    if RO in gpd_df.columns:
        pass
    else:
        print(gpd_df.columns)
        sys.exit(
            'Check RO user variable.\
            File does not contain the "' + RO + '" field.')
            
    if (gpd_df[RO].dtypes) == 'float':
        print ('Your specified river order column "', RO,
               '" is a float.  Please use an INT or a text field for your',
               'classification.')
        sys.exit()
    ls = []
    for x in (gpd_df[RO].unique()):
        ls.append(x)
    # make alpha numerical sort
    # ls = [2,'b',1,'a'] # sort test
    ls = alpha_sort(ls)
    print('River order alpha sort unique values list')
    print(ls,'\n')
    return ls
    
def read_shape_file_to_gpd_df(file_str, geometry_type):
    """
    Parameters
    ----------
    file_str : str
    geometry_type : str
        geometry options 'LineString' or 'Point'.

    Returns
    -------
    gpd_df : TYPE
        DESCRIPTION.

    Read shp file, make a geopandas dataframe
    and change the INFILE_LENGTH column name.
    Strip 3D geometries for lines and points
    
    gpd_df has all original data and fields except for 'infile_lenght' name 
    change

    Reading a gpkg may give the following warning:
        py:577: RuntimeWarning: Sequential read of iterator was interrupted.
        Resetting iterator. This can negatively impact the performance.
        for feature in features_lst:
        Warning is caused by gdal driver. Please ignore.

    """
    gpd_df = gpd.read_file(file_str)  # make a geopandas dataframe from file

    # Check if files contains collections (multipoints...)
    check_shapely_coll_message = (check_shapely_coll(gpd_df.geometry))
    if check_shapely_coll_message is None:  # changed form ==
        pass
    else:
        sys.exit(check_shapely_coll_message)
    if geometry_type == 'LineString':
        if INFILE_LENGTH in gpd_df.columns:
            gpd_df = gpd_df.rename(columns={INFILE_LENGTH: 'infile_length'})
            # print('line file')
        else:
            sys.exit(
                'Check INFILE_LENGTH user variable.\
                File does not contain the "' + INFILE_LENGTH + '" field.')

        # remove 3D from LineString
        gpd_df.geometry = (strip_3d(gpd_df.geometry))

    elif geometry_type == 'Point':
        check_unique_id(file_str)
        gpd_df.geometry = (strip_3d(gpd_df.geometry))  # remove 3D from Points
    else:
        print('Please specifiy geometry_type: Point or LineString')
        pass
    # print (gpd_df)
    return gpd_df

def check_shapely_coll(geom_obj):
    """
    Parameters
    ----------
    geom_obj : a geometry
    Returns : none or a message
    -------
        message : Check if geometry is not compatible with networks
        used by read_shape_file_to_gpd_df() function
    """

    for w in geom_obj:
        if w.geom_type == 'MultiPoint':
            message = ('Your file contains MultiPoint objects. Software cannot\
                       handle collections. Save file as single objects in \
                       a GIS.')

        elif w.geom_type == 'MultiLineString':
            message = ('Your file contains MultLineString objects. Software\
                       cannot handle collections objects. Save file as \
                       single objects in a GIS.')

        elif w.geom_type == 'MultiPolygon':
            message = ('Your file contains MultiPolygon objects. Software \
                       cannot handle collections Polygons. Save file as \
                       single objects lines in a GIS.')

        elif w.geom_type == 'Polygon':  
            message = ('Your file contains Polygon objects. Software cannot \
                       handle Polygons. Save file as single objects\
                       lines in a GIS.')
        else:
            message = None
    return message


def strip_3d(geom_obj):
    """
    This function will strip the 3D from the Point and LineString files.
    Used by the read_shape_file_to_gpd_df() function
    https://gist.github.com/rmania/8c88377a5c902dfbc134795a7af538d8

    TODO Replace strip_3d by wkt.dumps(pt, trim = False, output_dimension = 2)
    geom = shapely.wkb.loads(
        shapely.wkb.dumps(geom, output_dimension=2))
    """

    new_geo = []
    for w in geom_obj:
        if w.has_z:
            if w.geom_type == 'Point':
# =============================================================================
#                 # Rounding problem: part is ok. w.coords is
#                 # like wkt.dumps(w,trim = Flase) but not w.wkt.
#                 # Any rounding is caused when python float is created.
# =============================================================================
                xy_point = (w.x, w.y)
                # xy_point = (w.coords[0][0], w.coords[0][1])  # old
                xy_coord = Point(xy_point)
                new_geo.append(xy_coord)
            elif w.geom_type == 'LineString':
                xy_line = [xy[:2] for xy in list(w.coords)]
                xy_coord = LineString(xy_line)
                new_geo.append(xy_coord)
            else:
                print(w.geom_type)
                sys.exit('strip_3d geometry is not a Point or a LineString')
        else:
            new_geo.append(w)
    return new_geo

def check_unique_id(file_name: str):
    """
    Parameters
    ----------
    file_name : str
        Path to filename.

    Returns
    -------
    non_unique_ids : TYPE gdf
        DESCRIPTION.
        gdf of duplicate unique ID
    """
    all_ok = UNIQUE_ID + ' has been checked.' \
        + ' no duplicate values found.'
    input_point_gdf = gpd.read_file(file_name)
    id_in_file = UNIQUE_ID in input_point_gdf.columns
    if id_in_file is False:  
        sys.exit('"'+UNIQUE_ID + '" field is not found in '
                 + file_name
                 + '. Please verify UNIQUE_ID user variable')

    non_unique_ids = input_point_gdf.loc[
        input_point_gdf[UNIQUE_ID].duplicated()]

    if (len(non_unique_ids)) == 0:
        pass
    else:
        non_unique_ids_list = non_unique_ids[UNIQUE_ID].tolist()
        print(UNIQUE_ID+' contains the following duplicate value:')
        for v in non_unique_ids_list:
            print(v)
        print('Please select a field with unique values')
        sys.exit()
        return non_unique_ids
    return all_ok

def pickle_to_file(var, filenamestr):
    """
    Parameters
    ----------
    var : TYPE
        DESCRIPTION.
    filenamestr : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    """
    python_dict = os.path.join(
        PYTHO_LIST_DICT_VARIABLES_SUBDIRECTORY, BASIN+'_'+filenamestr)

    python_dict_f = open(python_dict, 'wb')
    pickle.dump(var, python_dict_f)
    python_dict_f.close()

def write_graph_to_csv(graph, egdes_file_str, nodes_file_str):
    """
    Parameters
    ----------
    graph : TYPE NetworkX graph

    egdes_file_str : str
        Name of the output file.
    nodes_file_str : str
        Name of the output file.

    Returns
    -------
    None.

    """
    egdes_file_str = egdes_file_str+'_WKT.csv'
    nodes_file_str = nodes_file_str+'_WKT.csv'

    # edges file
    pandas_df = nx.to_pandas_edgelist(graph, source='u', target='v',
                                      nodelist=None, dtype=None, order=None)
    geopandas_df = gpd.GeoDataFrame(pandas_df)
    geopandas_df.to_csv(egdes_file_str, sep=';',
                        index_label='id', mode='w',
                        header=True)

    nodelist = graph.nodes()
    pointlist = []
    # nodes file
    for x in nodelist:
        pointlist.append(Point(x))
    pandas_df = pd.DataFrame({'geometry': pointlist})
    geopandas_df = gpd.GeoDataFrame(pandas_df)
    geopandas_df
    geopandas_df.to_csv(nodes_file_str, sep=';',
                        index_label='id', mode='w',
                        header=True, quoting=None)

# =============================================================================
# def write_graph_to_gpkg(graph, egdes_file_str, nodes_file_str):
#     """
#     Parameters
#     ----------
#     graph : TYPE NetworkX graph
# 
#     egdes_file_str : str
#         Name of the output file.
#     nodes_file_str : str
#         Name of the output file.
# 
#     Returns
#     -------
#     None.
# 
#     # TODO like write_graph_to_csv but not currently used 
#     write Graph to csv with wkt.
#     proj4_epsg_32718 = "+proj=utm +zone=18 +south +ellps=WGS84 +datum=WGS84 \
#     #+units=m +no_defs" # crude, make a fonction for this.
#     Get epsg code from input
#     # TODO  Implement this function in future code 
#     """
#     # edges file
#     pandas_df = nx.to_pandas_edgelist(graph, source='u', target='v',
#                                       nodelist=None, dtype=None, order=None)
#     geopandas_df = gpd.GeoDataFrame(pandas_df)
#     geopandas_df.crs = {'init': 'epsg:32718'}  # TODO Deprecated, find epsg from network file
#     geopandas_df.to_csv(egdes_file_str, sep=';',
#                         index_label='id', mode='w',
#                         header=True)
# 
#     nodelist = graph.nodes()
#     pointlist = []
#     # nodes file
#     for x in nodelist:
#         pointlist.append(Point(x))
#     pandas_df = pd.DataFrame({'geometry': pointlist})
#     geopandas_df = gpd.GeoDataFrame(pandas_df)
#     geopandas_df
#     geopandas_df.crs = {'init': 'epsg:32718'}  # TODO Deprecated, find epsg from network files
#     geopandas_df.to_csv(nodes_file_str, sep=';',
#                         index_label='id', mode='w',
#                         header=True, quoting=1)
# 
# =============================================================================

def write_new_edges_dct_to_csv(dct, classify_by_RO = CLASSIFY_BY_RIVER_ORDER, ro = RO):
    """
    0: split type
    1: node wkt
    2: Entry_cost_edge (u,v)
    3: Entry_cost_edge length
    4: Entry_cost_edge wkt
    5: (u,v) First part of line (or all the line)
    6: (length)First part of line (or all the line)
    7: (wkt) First part of line (or all the line)
    8: (u,v) Second part of the line (if line was split)
    9: (length)Second part of the line (if line was split)
    10: (wkt) Second part of the line (if line was split)
    11: nearest_line_uv_tup Nearest line in the Graph.
    12: nearest_line_river_order
    """
    # write python dict to file

    outside_node = os.path.join(
        OUTPUT_TIE_OUTSIDE_NODE_SUBDIRECTORY, BASIN +
        '_source_target_nodes_WKT.csv')

    new_edges_dct_driveways = os.path.join(
        OUTPUT_TIE_OUTSIDE_NODE_SUBDIRECTORY, BASIN +
        '_network_entry_cost_edges_WKT.csv')

    new_edges_dct_split0 = os.path.join(
        OUTPUT_TIE_OUTSIDE_NODE_SUBDIRECTORY, BASIN +
        '_receiving_edges_split0_WKT.csv')

    new_edges_dct_split1 = os.path.join(
        OUTPUT_TIE_OUTSIDE_NODE_SUBDIRECTORY, BASIN +
        '_receiving_edges_split1_WKT.csv')

    outside_node_f = open(outside_node, 'w')
    new_edges_dct_driveways_f = open(new_edges_dct_driveways, 'w')
    new_edges_dct_split0_f = open(new_edges_dct_split0, 'w')
    new_edges_dct_split1_f = open(new_edges_dct_split1, 'w')

    # header 
    if classify_by_RO is False:
        new_edges_dct_driveways_f.write('split_type;node_wkt;uv;'+str(INFILE_LENGTH)+';wkt')
    elif classify_by_RO is True:
        new_edges_dct_driveways_f.write('split_type;node_wkt;uv;'+str(INFILE_LENGTH)+';wkt;'+str(ro)+'_of_nearest_edge')
    new_edges_dct_driveways_f.write('\n')

    if classify_by_RO is False:
        new_edges_dct_split0_f.write('split_type;uv;'+str(INFILE_LENGTH)+';wkt')
    elif classify_by_RO is True:
        new_edges_dct_split0_f.write('split_type;uv;'+str(INFILE_LENGTH)+';wkt;'+str(ro))
    new_edges_dct_split0_f.write('\n')
    
    if classify_by_RO is False:
        new_edges_dct_split1_f.write('split_type;uv;'+str(INFILE_LENGTH)+';wkt')
    elif classify_by_RO is True:
        new_edges_dct_split1_f.write('split_type;uv;'+str(INFILE_LENGTH)+';wkt;'+str(ro))
    new_edges_dct_split1_f.write('\n')
    
    outside_node_f.write('wkt;type')
    outside_node_f.write('\n')

    for key in dct:
        # write driveways
        outside_node_f.write(dct[key][1])
        outside_node_f.write(';')
        outside_node_f.write('source and target nodes')
        outside_node_f.write('\n')

        new_edges_dct_driveways_f.write(str(dct[key][0]))
        new_edges_dct_driveways_f.write(';')
        new_edges_dct_driveways_f.write(str(dct[key][1]))
        new_edges_dct_driveways_f.write(';')
        new_edges_dct_driveways_f.write(str(dct[key][2]))
        new_edges_dct_driveways_f.write(';')
        new_edges_dct_driveways_f.write(str(dct[key][3]))
        new_edges_dct_driveways_f.write(';')
        new_edges_dct_driveways_f.write(str(dct[key][4]))
        if classify_by_RO is True:
            new_edges_dct_driveways_f.write(';')
            new_edges_dct_driveways_f.write(str(dct[key][12])) 
        new_edges_dct_driveways_f.write('\n')

        new_edges_dct_split0_f.write(str(dct[key][0]))
        new_edges_dct_split0_f.write(';')
        new_edges_dct_split0_f.write(str(dct[key][5]))
        new_edges_dct_split0_f.write(';')
        new_edges_dct_split0_f.write(str(dct[key][6]))
        new_edges_dct_split0_f.write(';')
        new_edges_dct_split0_f.write(str(dct[key][7]))
        if classify_by_RO is True:
            new_edges_dct_split0_f.write(';')
            new_edges_dct_split0_f.write(str(dct[key][12])) 
        new_edges_dct_split0_f.write('\n')

        new_edges_dct_split1_f.write(str(dct[key][0]))
        new_edges_dct_split1_f.write(';')
        new_edges_dct_split1_f.write(str(dct[key][8]))
        new_edges_dct_split1_f.write(';')
        new_edges_dct_split1_f.write(str(dct[key][9]))
        new_edges_dct_split1_f.write(';')
        new_edges_dct_split1_f.write(str(dct[key][10]))
        if classify_by_RO is True:
            new_edges_dct_split1_f.write(';')
            new_edges_dct_split1_f.write(str(dct[key][12])) 
        new_edges_dct_split1_f.write('\n')

    outside_node_f.close()
    new_edges_dct_driveways_f.close()
    new_edges_dct_split0_f.close()
    new_edges_dct_split1_f.close()

def write_testing_report(report_list):
    year, month, day, hour, minutes, seconds = map(int, time.strftime("%Y %m %d %H %M %S").split())
    testing_report = os.path.join(
        TESTING_MODE_SUBDIRECTORY +'_testing_mode_report_'+str(year)+str(month)
        +str(day)+str(hour)+str(minutes)+str(seconds)+'.csv')
    testing_report_f = open(testing_report, 'w')
    for x in report_list:
        testing_report_f.write(str(x))
        testing_report_f.write('\n')
    testing_report_f.close()

def write_route_dct_to_file(
        file_str, header_str, fileid_str, file_write_mode, dct,
        closeoutputfile):
    """Used by def calculate_routes(). Returns empty dct. WRITE MODE MUST
    BE 'a' for append. """

    if file_write_mode == 'a':
        pass
    else:
        sys.exit('Check file write mode in calculate_route()')
    if os.path.isfile(file_str):  # write head if output file does not exist
        outputfile = open(file_str, file_write_mode)
    else:
        outputfile = open(file_str, file_write_mode)
        outputfile.write(str(header_str))
    counter = 0

    sorted_keys = sorted(list(dct))
    
    # csv_n = (len((dct[sorted_keys[0]][0]).split(";"))) # old
 
    csv_n = (len((dct[sorted_keys[0]]))) #  new v2


# =============================================================================
# old  # data is no longer in a single str
#   for k in sorted_keys:
#         for q in (dct[k][0]).split(';'):            
#             if counter < csv_n-1:
#                 outputfile.write(q)
#                 outputfile.write(';')
#                 counter += 1
#             else:
#                 outputfile.write(q)
#                 outputfile.write('\n')
#                 counter = 0
# =============================================================================

# =============================================================================
# New for v2 
# =============================================================================

    for k in sorted_keys:
        for q in (dct[k]):            
            if counter < csv_n-1:
                outputfile.write(q)
                outputfile.write(';')
                counter += 1
            else:
                outputfile.write(q)
                outputfile.write('\n')
                counter = 0

    if closeoutputfile is True:
        outputfile.close()
    dct = defaultdict(list)
    return dct

def make_graph_from_gpd_df(gpd_df):

    """
    Parameters
    ----------
    gpd_df : TYPE GeoPandas Data Frame
        The network file
    Returns : directed_graph, undirected_graph, gpd_df_no_paralle_edges
    -------
     Must go from DiGraph to Graph
     
     New in v2, added River order
    
    -Used by calculate_routes():"""
    
# =============================================================================
#     Make new gpd_df with no paralle edges.
# =============================================================================
    
# =============================================================================
#                                                 old
#     gpd_df_no_paralle_edges = gpd.GeoDataFrame(columns=['infile_length','geometry']) 
#     gpd_df_no_paralle_edges = gpd.GeoDataFrame(columns=['infile_length',str(RO),'geometry']) # new v2
# =============================================================================
    col = list(gpd_df.columns)
    gpd_df_no_paralle_edges = gpd.GeoDataFrame(columns=col) # new v2
    
    # print(col)    
    # print('gpd_df.head\n')
    # print(gpd_df.columns)
    # print ('gpd_df_no_paralle_edges.\n')
    # print(gpd_df_no_paralle_edges.columns)
    
    # print (gpd_df.head)
    # print ('print code line 675 ',gpd_df.columns,'\n')
    directed_graph = nx.DiGraph()
    for lines in range(0, len(gpd_df)): # index is a int 
        u = gpd_df.geometry[lines].coords[0]  # x,y coords of first node
        v = gpd_df.geometry[lines].coords[-1]  # x,y coords of last node

        if directed_graph.has_edge(u, v):
            length = gpd_df.loc[lines,'infile_length']
#            print (length)
            cutintwo = cut(gpd_df.geometry[lines], (length/2))
            for parts in range(0, len(cutintwo)):
                geom = cutintwo[parts]
                u1 = cutintwo[parts].coords[0]
                v2 = cutintwo[parts].coords[-1]
                half_infile_length = length/2

                directed_graph.add_edge(u1, v2)
                # edges to graph with all the fields
                for c in col:
                    directed_graph.edges[u1, v2][c] = (gpd_df.loc[lines][c])
                # then correct edge with new geometry and info
                directed_graph.edges[u1, v2]['geometry'] = geom
                directed_graph.edges[u1, v2]['infile_length'] = half_infile_length  # Length must be file variable
                
                # All original values to a dict
                gpd_line_dict = gpd_df.loc[lines].to_dict()
                # Correct values for geometry and length
                gpd_line_dict['geometry'] = geom
                gpd_line_dict['infile_length'] = half_infile_length
                gpd_df_no_paralle_edges.loc[len(gpd_df_no_paralle_edges)] = list(gpd_line_dict.values())
            

        elif directed_graph.has_edge(v, u):
            # Spilt geometry in 2 at half way point.
            # length = gpd_df.length[lines]
            length = gpd_df.loc[lines,'infile_length']
            cutintwo = cut(gpd_df.geometry[lines], (length/2))
            for parts in range(0, len(cutintwo)):
                geom = cutintwo[parts]
                u1 = cutintwo[parts].coords[0]
                v2 = cutintwo[parts].coords[-1]
                half_infile_length = length/2
                directed_graph.add_edge(u1, v2)
                # edges to graph with all the fields
                for c in col:
                    directed_graph.edges[u1, v2][c] = (gpd_df.loc[lines][c])
                # then correct edge with new geometry and info
                directed_graph.edges[u1, v2]['geometry'] = geom
                directed_graph.edges[u1, v2]['infile_length'] = half_infile_length  # Length must be file variable

                # All original values to a dict
                gpd_line_dict = gpd_df.loc[lines].to_dict()
                # Correct values for geometry and length
                gpd_line_dict['geometry'] = geom
                gpd_line_dict['infile_length'] = half_infile_length
                gpd_df_no_paralle_edges.loc[len(gpd_df_no_paralle_edges)] = list(gpd_line_dict.values())

        else:

            # new v2
            directed_graph.add_edge(u, v)
            # edges to graph with all the fields
            for c in col:
                directed_graph.edges[u, v][c] = (gpd_df.loc[lines][c])
                
            gpd_df_no_paralle_edges.loc[len(gpd_df_no_paralle_edges)] = gpd_df.loc[lines]
            
            
    # make an undirected graph
    undirected_graph = directed_graph.to_undirected()
    return directed_graph, undirected_graph, gpd_df_no_paralle_edges

def cut(line, distance):
    """# Cuts a line in two at a distance from its starting point
    https://sgillies.net/2010/07/06/shapely-recipes.html.

    Used by def make_graph_from_gpd_df(gpd_df):
    """

    if distance <= 0.0 or distance >= line.length:
        return [LineString(line)]
    coords = list(line.coords)
    for i, p in enumerate(coords):
        pd = line.project(Point(p))
        if pd == distance:
            return [
                LineString(coords[:i+1]),
                LineString(coords[i:])]
        if pd > distance:
            cp = line.interpolate(distance)
            return [
                LineString(coords[:i] + [(cp.x, cp.y)]),
                LineString([(cp.x, cp.y)] + coords[i:])]

def check_network_graph(dg,ug):
    """
    Used for testing and checking graph nodes and edges.
    
    Parameters
    ----------
    dg : TYPE
        DESCRIPTION.
    ug : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    """    
 
    print('Number of Nodes in DG =', len(dg.nodes))
    print('Number of Edges in DG =', len(dg.edges))
    print('Edge keys in DG: ',list(list(dg.edges(data=True))[0][-1].keys()))
    print('Number of Nodes in UG =', len(ug.nodes))
    print('Number of Edges in UG =', len(ug.edges))
    print('Edge keys in UG: ',list(list(ug.edges(data=True))[0][-1].keys()))
    print('***************')


def tie_outside_node(gpd_df_nodes, gpd_df_network, graph, classify_by_RO = CLASSIFY_BY_RIVER_ORDER):
    """
    Parameters
    ----------
    gpd_df_nodes : TYPE
        DESCRIPTION: gpd_df_nodes = nodes to tie
    gpd_df_network : TYPE
        DESCRIPTION: gpd_df_network = the network with no parallel edges
    graph : TYPE
        DESCRIPTION: networkX graph
    Returns
    -------
    new_edges_dct : TYPE defaultdict
        DESCRIPTION.
            key = source_target_nodes_geom

            Dictionary index
            
            0: split type # Help with trouble shooting
            1: node wkt
            2: Entry_cost_edge (u,v)
            3: Entry_cost_edge length
            4: Entry_cost_edge wkt
            5: (u,v) First part of line (or all the line)
            6: (length)First part of line (or all the line)
            7: (wkt) First part of line (or all the line)
            8: (u,v) Second part of the line (if line was split)
            9: (length)Second part of the line (if line was split)
            10: (wkt) Second part of the line (if line was split)
            11: nearest_line_uv_tup Nearest line in the Graph.
            12: nearest_line_river_order # same for all parts and entry_cost_edges


    For each outside node, finds the nearest line inside the buffer. The buffer
    grows if no lines are found. An entry cost edge is created and the nearest
    line is split in one (no split) to two parts.
    """

    new_edges_dct = defaultdict(list)

    # GPD_DF_EDGES_NETWORK.geometry[0] better?
    # print(gpd_df_nodes.columns)
    
    for index, row in gpd_df_nodes.iterrows():

        source_target_nodes_geom = row.geometry
        nearest_line = (geopanda_min_dist(row.geometry, gpd_df_network, 1000))
        
        # print(824,nearest_line) #  nearest_line has orginal columns.
        
        nearest_line_geometry = nearest_line.geometry
        # Extract untrimmed coord of first and last nodes.
        u = nearest_line_geometry.coords[0]
        v = nearest_line_geometry.coords[-1]
        nearest_line_uv_tup = (u, v)
        nearest_line_infile_length = nearest_line.infile_length
        if classify_by_RO is True:
            nearest_line_river_order = nearest_line[RO]
        # print (nearest_line_river_order)
        
        if ((source_target_nodes_geom.x, source_target_nodes_geom.y)) in graph.nodes():
            # If the source or target is in the Graph, no need to modify graph
            # print((source_target_nodes_geom.x,source_target_nodes_geom.y), index)
            # print ('in Graph','\n')
            
            splited = 0
            # join_line
            uv_tup0 = None
            wkt0 = None
            length0 = None
            # line split 1
            uv_tup1 = None
            wkt1 = None
            length1 = None
            # line split 2
            uv_tup2 = None
            wkt2 = None
            length2 = None
           
            pass

        else:
            # print ('Point not found in graph. Index: ', index, '\n')
            # Find nearest point on this nearest line, find possible join_line.
            # This line will intersect only if there is a node on the line.

# =============================================================================
#             # old code wkt = trimmed coord. Keep as reminder
#             outside_node_grid_geom_to_nearest_p = [q.wkt for q in nearest_points(source_target_nodes_geom,nearest_line_geometry)]  # returns 2 points
#             print (outside_node_grid_geom_to_nearest_p, 'test line 763')
# =============================================================================
            outside_node_grid_geom_to_nearest_p = [wkt.dumps(q, trim = False) for q in nearest_points(source_target_nodes_geom,nearest_line_geometry)]  # returns 2 points
            # load point 2 (nearest point)
            nearest_p = loads(outside_node_grid_geom_to_nearest_p[1])
            # join_line = (LineString([source_target_nodes_geom,nearest_p]))
            # Make a coodinate tuple. Check if its a first or last node
            # nearest_p_tup = (nearest_p.x, nearest_p.y) # TODO never used
            near_first_node = nearest_p.distance(
                (Point(nearest_line_geometry.coords[0])))

            near_last_node = nearest_p.distance(
                (Point(nearest_line_geometry.coords[-1])))

            if near_first_node <= near_last_node:
                nearest_p_case1 = (Point(nearest_line_geometry.coords[0]))
            else:
                nearest_p_case1 = (Point(nearest_line_geometry.coords[-1]))
            if near_first_node <= END_NODE_TOLERANCE or near_last_node <= END_NODE_TOLERANCE:  # if problem with float point, geometry will be split in one piece only, not 2.
            # nearest_p_to_node0_distance = nearest_p.distance(loads('Point ('+ str(nearest_line_geometry.coords[0][0])+' '+str(nearest_line_geometry.coords[0][1])+')'))
            # nearest_p_to_lastnode_distance = nearest_p.distance(loads('Point ('+ str(nearest_line_geometry.coords[-1][0])+' '+str(nearest_line_geometry.coords[-1][1])+')'))
            # if nearest_p_to_node0_distance < 0.001 or nearest_p_to_lastnode_distance < 0.001:

                splited = 1  # first node
                # print (nearest_p_case1)
                join_line_exact_case1 = (
                    LineString([source_target_nodes_geom, nearest_p_case1]))
                # print (join_line_exact_case1,'JOIN LINE')
                # print (join_line_exact_case1.coords[0])
                # print (join_line_exact_case1.coords[-1])
                uv_tup0 = (
                    join_line_exact_case1.coords[0],
                    join_line_exact_case1.coords[-1])

                # print (uv_tup0,'UV TUP0')
# =============================================================================
#                 # old BUG Trims the geometry. Keep as reminder.
#                 wkt0 = join_line_exact_case1.wkt
# =============================================================================
                wkt0 = wkt.dumps(join_line_exact_case1, trim = False)

                length0 = 0.0
                # line split 1
                uv_tup1 = nearest_line_uv_tup
                wkt1 = wkt.dumps(nearest_line_geometry, trim = False)
                length1 = nearest_line_infile_length
                # line split 2

                uv_tup2 = None
                wkt2 = None
                length2 = None

            else:
                join_line = (LineString([source_target_nodes_geom, nearest_p]))
                # print ('nearest_p is not a first or last node.')
                splits_edges_coll = []
                # grow 1 mm at a time
                start_buf_ratio = 1+(0.001/(join_line.length))
                # Break out of endless loop
                break_count = 0
                while (len(splits_edges_coll)) != 2:
                    # Grow the line until it splits
                    # if outside_node_grid_index == 87:
                    #     print (source_target_nodes_geom, nearest_p)

                    join_line_interpolation = get_extrapoled_line(
                        source_target_nodes_geom, nearest_p, start_buf_ratio)

                    splits_edges_coll = split(
                        nearest_line_geometry, join_line_interpolation)
                    start_buf_ratio += (1/(join_line.length))
                    break_count += 1
                    if break_count == 1000:
                        frameinfo = getframeinfo(currentframe())+1
                        sys.exit(
                            'Did over 1000 loops.  Cannot split line.\
                            Go to code line ', frameinfo)

                    # problem node testing keep this
                    # if outside_node_grid_index == 87:
                    #     print (source_target_nodes_geom, nearest_p)
                    #     print (join_line_interpolation)
                    # print('Increasings join line intepolation')
                # print(outside_node_grid_index)
                # print('line was cut in', str(len(splits_edges_coll)),'segments')
                splited = 2  # line splited in 2
                # Select last node in geom 0 of splits_edges_coll as
                nearest_p_exact_case2 = splits_edges_coll[0].coords[-1]

                join_line_exact_case2 = (
                    LineString(
                        [source_target_nodes_geom, nearest_p_exact_case2]))

                uv_tup0 = (
                    join_line_exact_case2.coords[0],
                    join_line_exact_case2.coords[-1])
                length0 = 0.0
                wkt0 = wkt.dumps(join_line_exact_case2, trim = False)
                # line split 1
                uv_tup1 = (
                    splits_edges_coll[0].coords[0],
                    splits_edges_coll[0].coords[-1])

                length1 = ((splits_edges_coll[0].length /
                            (splits_edges_coll[0].length +
                             splits_edges_coll[1].length)) *
                           nearest_line_infile_length)
                wkt1 = wkt.dumps(splits_edges_coll[0], trim = False)

                # line split 2
                uv_tup2 = (splits_edges_coll[1].coords[0],
                           splits_edges_coll[1].coords[-1])

                length2 = ((splits_edges_coll[1].length /
                            (splits_edges_coll[0].length +
                             splits_edges_coll[1].length)) *
                           nearest_line_infile_length)

                wkt2 = wkt.dumps(splits_edges_coll[1], trim = False)
        # 0: split type
        new_edges_dct[(source_target_nodes_geom.x,
                       source_target_nodes_geom.y)].append(splited)
        # 1: node wkt
        new_edges_dct[(source_target_nodes_geom.x,
                       source_target_nodes_geom.y)].append(
                           wkt.dumps(source_target_nodes_geom, trim = False))
        # 2: Entry_cost_edge (u,v)
        new_edges_dct[(source_target_nodes_geom.x,
                       source_target_nodes_geom.y)].append(uv_tup0)
        # 3: Entry_cost_edge length
        new_edges_dct[(source_target_nodes_geom.x,
                       source_target_nodes_geom.y)].append(length0)
        # 4: Entry_cost_edge wkt
        new_edges_dct[(source_target_nodes_geom.x,
                       source_target_nodes_geom.y)].append(wkt0)
        # 5: First part of line (or all the line)
        new_edges_dct[(source_target_nodes_geom.x,
                       source_target_nodes_geom.y)].append(uv_tup1)
        # 6: First part of line (or all the line)
        new_edges_dct[(source_target_nodes_geom.x,
                       source_target_nodes_geom.y)].append(length1)
        # 7: First part of line (or all the line)
        new_edges_dct[(source_target_nodes_geom.x,
                       source_target_nodes_geom.y)].append(wkt1)
        # 8: Second part of the line (if line was split)
        new_edges_dct[(source_target_nodes_geom.x,
                       source_target_nodes_geom.y)].append(uv_tup2)
        # 9: Second part of the line (if line was split)
        new_edges_dct[(source_target_nodes_geom.x,
                       source_target_nodes_geom.y)].append(length2)
        # 10: Second part of the line (if line was split)
        new_edges_dct[(source_target_nodes_geom.x,
                       source_target_nodes_geom.y)].append(wkt2)
        # 11: nearest_line_uv_tup
        new_edges_dct[(source_target_nodes_geom.x,
                       source_target_nodes_geom.y)].append(
                           nearest_line_uv_tup)
        # 12: nearest_line_river_order
        if classify_by_RO is True:
            new_edges_dct[(source_target_nodes_geom.x,
                            source_target_nodes_geom.y)].append(
                                nearest_line_river_order)
                           
        # 13 trouble shooting only
        # new_edges_dct[(source_target_nodes_geom.x,
        # source_target_nodes_geom.y)].append(nearest_line)

    print('tie_outside_node(): n sites added = ', len(new_edges_dct))
    return new_edges_dct


def geopanda_min_dist(point, gpd_dataframe, initial_buffer=1000):
    """
    Parameters
    ----------
    point : TYPE Shapely point
        DESCRIPTION.
    gpd_dataframe : TYPE DataFrame
        DESCRIPTION.
    initial_buffer : TYPE, optional
        DESCRIPTION. The default is 1000.
    Returns
    -------
    geoseries : TYPE Shapely geoseries
        DESCRIPTION.

    https://gis.stackexchange.com/questions/266730/filter-by-bounding-box-in-geopandas/266833
    Use the .cx index to find geoms inside the bounding box.
    Only use calculate the distance based on that.  Increase the bounding
    box size is you don't have a geometry.
    Return THE nearest geometry from point a point
    """
    buffer_steps = 1000
    xmin, ymin, xmax, ymax = (point.buffer(initial_buffer)).bounds
    # use the .cx indexer to find all possible geom in bounding box
    gpd_df = gpd_dataframe.cx[xmin:xmax, ymin:ymax]

    # if empty, go into loop
    while gpd_df.empty:
        # print ('Increasing buffer size')# Testing
        xmin, ymin, xmax, ymax = (point.buffer(initial_buffer)).bounds
        # use the .cx indexer to find all possible geom in bounding box
        gpd_df = gpd_dataframe.cx[xmin:xmax, ymin:ymax]
        initial_buffer = initial_buffer + buffer_steps
    gpd_df = gpd_df.copy(deep=True)

    gpd_df['Dist'] = gpd_df.apply(
        lambda row:  point.distance(row.geometry), axis=1)

    geoseries = gpd_df.loc[gpd_df['Dist'].idxmin()]
    # print (geoseries)
    return geoseries



def distance_matrix_route(gpd_df, gpd_df_destination, output_file_path='list',
                                use2files = USE_SOURCE_AND_TARGET_FILES,
                                upper_half = CALCULATE_UPPER_HALF_MATRIX_ONLY):
    """Calculate an upper difference matrix
    -Used to make file for grouper()
    -Used to make a list for calculate_routes()
    output_file_path= list is no longeur used
    Hidden dependencies:
    PYTHO_LIST_DICT_VARIABLES_SUBDIRECTORY
    BASIN
    v2: Can now calculate full matrix for one file
    """
    if use2files is True:
        upper_half = False
    
    if use2files is False:
        a = gpd_df.to_dict(orient='index')
        b = gpd_df.to_dict(orient='index')
        
    elif use2files is True:
        a = gpd_df.to_dict(orient='index')
        b = gpd_df_destination.to_dict(orient='index')
        
    upper_matrix = deque()
    lineid = 0

    for q in a:
        # sourceid = (a[q]['gridcode'])
        sourceid = (a[q][UNIQUE_ID])
        sourcegeom = (a[q]['geometry'])
        for w in b:
            # targetid = (b[w]['gridcode'])
            targetid = (b[w][UNIQUE_ID])
            targetgeom = (b[w]['geometry'])

            if upper_half is True:
                if q == w:
                    pass
                else:
                    outputstring = (
                        str(lineid) + ',' + str(sourceid) + ',' + str(targetid) +
                        ',' + wkt.dumps(sourcegeom) + ',' + wkt.dumps(targetgeom))
                    upper_matrix.append(outputstring)
                    lineid += 1
            elif upper_half is False:
                outputstring = (
                    str(lineid) + ',' + str(sourceid) + ',' + str(targetid) +
                    ',' + wkt.dumps(sourcegeom) + ',' + wkt.dumps(targetgeom))
                upper_matrix.append(outputstring)
                lineid += 1
            
            else:
                sys.exit(
                    'Select True or False for ',
                    'CALCULATE_UPPER_HALF_MATRIX_ONLY')

        if upper_half is True: # new v2
            b.pop(list(b)[0])
        elif upper_half is False: # new v2
            pass

    upper_distance_matrix_str = os.path.join(
        PYTHO_LIST_DICT_VARIABLES_SUBDIRECTORY, BASIN +
        '_source_target_routes_to_calculate_deque.csv') 
    upper_distance_matrix_f = open(upper_distance_matrix_str, 'w')

    for x in upper_matrix:
        upper_distance_matrix_f.write(x)
        upper_distance_matrix_f.write('\n')
    upper_distance_matrix_f.close()

    print('Upper_distance_matrix calculated. Number of querries to do =',
          len(upper_matrix))
    return upper_matrix

def grouper(iterable, chunksize, fillvalue=None):
    "Collect data into fixed-length chunks or blocks"
    # grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx
    args = [iter(iterable)] * chunksize
    return zip_longest(fillvalue=fillvalue, *args)

def get_extrapoled_line(p1, p2, extrapol_ratio):
    """Creates a line extrapoled in p1->p2 direction
    Input is shapely objects
    """
    # print('extrapolated')
    c = (p1.x+extrapol_ratio*(p2.x-p1.x), p1.y+extrapol_ratio*(p2.y-p1.y))
    return LineString([p1, c])

def resplit_edge(source_node, target_node, source_id, target_id, include_wkt):
    """
    Parameters
    ----------
    source_node : TYPE new_edges_dct value
         DESCRIPTION.
    target_node : TYPE new_edges_dct value
        DESCRIPTION.
    source_id : TYPE int
        DESCRIPTION.
    target_id : TYPE int
        DESCRIPTION.
    include_wkt : TYPE bool True or False
        DESCRIPTION.

    Returns
    -------
    None.

    """
    # print(source_node,'\n', target_node,'\n', source_id,'\n', target_id,'\n', include_wkt )
    # sys.exit()

    # resplit_new_edges_dct = defaultdict(list)
    # source_new_u1 = source_node[2][0]
    source_new_v1 = source_node[2][1]
    source_geom1 = loads(source_node[4])  # Entry_cost_edge
    # source_len1 = source_node[3]  # 0

    # source_new_u2 = source_node[5][0]
    # source_new_v2 = source_node[5][1]
    source_geom2 = loads(source_node[7])  # fist half of closest edge
    source_len2 = source_node[6]

    # source_new_u3 = source_node[8][0]
    # source_new_v3 = source_node[8][1]
    source_geom3 = loads(source_node[10])  # second half closest edge
    source_len3 = source_node[9]

    target_new_u1 = target_node[2][0]  # Entry_cost_edge2 # (554500.0, 9371500.0)
    target_new_v1 = target_node[2][1] # (554404.5979815992, 9371736.431023333)
    target_geom1 = loads(target_node[4]) # 'LINESTRING (554500.0000000000000000 9371500.0000000000000000, 554404.5979815991595387 9371736.4310233332216740)'
    # target_len1 = target_node[3]
    # print(source_id,target_id)

#   test to find closest geom
    if source_geom2.distance(target_geom1) < source_geom3.distance(
            target_geom1):
        nearest_line_geometry = source_geom2
        nearest_line_infile_length = source_len2
    else:
        nearest_line_geometry = source_geom3
        nearest_line_infile_length = source_len3

    # get Entry_cost_edge2 points

# =============================================================================
# # old 
#     source_target_nodes_geom = loads(
#         'Point ('+str(target_new_u1[0])+' '+str(target_new_u1[1])+')')
#
# =============================================================================
    source_target_nodes_geom = Point(target_new_u1)

# =============================================================================
# # old
#     nearest_p = loads(
#         'Point ('+str(target_new_v1[0])+' '+str(target_new_v1[1])+')')
# =============================================================================
    nearest_p = Point(target_new_v1)

    join_line = target_geom1
    splits_edges_coll = []
    start_buf_ratio = 1+(0.001/(join_line.length))  # grow 1 mm at a time
    join_line_interpolation = get_extrapoled_line(
        source_target_nodes_geom, nearest_p, start_buf_ratio)
    while (len(splits_edges_coll)) != 2:
        # Grow the line until it splits. Results = 2 lines
        # if there is a problem, This will be found during tie_outside_node()

        join_line_interpolation = get_extrapoled_line(
            source_target_nodes_geom, nearest_p, start_buf_ratio)
        splits_edges_coll = split(
            nearest_line_geometry, join_line_interpolation)

        start_buf_ratio += (0.001/(join_line.length))

    # see if (len(splits_edges_coll)) could be == 1 because of float point error:
    # #Find which line to keep
    # if len(splits_edges_coll) == 1:
    #     print('problem','\n')
    #     print (source_node[12])
    #     print(target_node[12])
    #     print(source_id,'source_id')
    #     print(target_id,'target_id')
    #     print(splits_edges_coll[0], 'split collection geom 1')

#elif len(splits_edges_coll) == 2:
    split_segment_0 = (splits_edges_coll[0])
    split_segment_1 = (splits_edges_coll[1])

    if source_geom1.distance(
            split_segment_0) < source_geom1.distance(split_segment_1):

        # test to find closest geom
        segment_to_keep = split_segment_0
        # length1 = ((splits_edges_coll[0].length / (splits_edges_coll[0].length+splits_edges_coll[1].length))* nearest_line_infile_length)
        length1 = ((split_segment_0.length / (split_segment_0.length + split_segment_1.length)) * nearest_line_infile_length)
    else:
        segment_to_keep = split_segment_1
        # no split_segment_cartesian_length_to_keep = split_segment_1.length
        length1 = ((split_segment_1.length / (split_segment_0.length+split_segment_1.length))*nearest_line_infile_length)

    # find if source is the 0 node or -1 node and find direction
    if segment_to_keep.coords[0] == source_new_v1:
        # Direction = Downstream

        downstream_length = length1
        total_length = downstream_length
        downstream_pct = 1.0
        upstream_pct = 0.0
        upstream_length = 0.0
        join_line2v = segment_to_keep.coords[-1]

    else:
        # segment_to_keep.coords[-1] == source_new_v1:
        # Direction = Upstream

        upstream_length = length1
        total_length = upstream_length
        downstream_pct = 0.0
        upstream_pct = 1.0
        downstream_length = 0.0
        join_line2v = segment_to_keep.coords[0]

    # Create route wkt
    if include_wkt == True:
        shapely_route = []
        shapely_route.append(source_geom1)
        shapely_route.append(segment_to_keep)

# =============================================================================
# # old
#         join_line2 = loads(
#             'LineString ('+str(target_new_u1[0])
#             + ' ' + str(target_new_u1[1]) + ','
#             + str(join_line2v[0]) + ' ' + str(join_line2v[1]) + ')')
#         print(wkt.dumps(join_line2))
# =============================================================================
        join_line2 = LineString([target_new_u1,join_line2v])
        shapely_route.append(join_line2)
        route_geometry = cascaded_union(shapely_route)
    else:
        route_geometry = None
    return downstream_pct, upstream_pct, downstream_length, upstream_length, total_length, route_geometry


def calculate_routes(input_list, classify_by_RO = CLASSIFY_BY_RIVER_ORDER, include_wkt = INCLUDE_WKT_IN_ROUTES):

    """WARNING HIDDEN DEPENDENCIES ON GLOBAL VARIABLE DG, UG,NEW_EDGES_DCT,
    ROUTE_RESULT_DCT_MAX_SIZE

    input_list = GROUPERLST = Upper_matrix_route_list
    """
    message = 'calculate_routes messages:'
    dg = deepcopy(DG)
    ug = deepcopy(UG)
    new_edges_dct = deepcopy(NEW_EDGES_DCT)  # key = Node, val =
    results_dict = defaultdict(list)
    fileid = input_list[0].split(',')[0]
    message = message, 'first line id: ', fileid
    outputfile_str = os.path.join(
        OUTPUT_ROUTE_SUBDIRECTORY, 'routes_' + BASIN + '_' + str(fileid) + '.csv')
    outputfile_write_mode = 'a' #IMPORTANT: MUST USE APPEND CHANGE FOR TESTING ONLY
    noroutefound = 0
    routefound = 0
    # egdes_not_in_graph = 0
    # For testing ERRASE OUTPUT FILE BEFORE STARTING
    try:
        os.remove(outputfile_str)
    except FileNotFoundError:
        pass

    # LIST FOR TESTING ONLY
    # input_list =["0,1,2,POINT (534379.2437082346 9837232.136086497),POINT(535419.0709327918 9833092.139250379)","1,2,3,POINT (534379.2437082346 9837232.136086497),POINT (541431.465314591 9835032.425044311)"]


    # Header with adaptable river order variable
    header = "inputline_id;source;target;source_xy;target_xy;downstream_pct;upstream_pct;downstream_length;upstream_length;total_length\n"
    if include_wkt is True:
        header = header[0:-1]+";wkt\n"
    #  Test to see if RO is set to none or if no data entries exists.
   
    if classify_by_RO is True:
        header = header[0:-1]
        for x in RIVER_ORDER_LST: # x is always a str
            header = header+';'+'downstream_ro'+x+'_length'
        for x in RIVER_ORDER_LST:
            header = header+';'+'upstream_ro'+x+'_length'
        header = header +'\n'
        # print ('\n',header)
    else:
        classify_by_RO = False

    for input_string in input_list:  # Grouper function can return Nonetype.
        if input_string is None:
            pass
        else:
            if TESTING_MODE is True:
                REPORT_LIST.append(input_string+';'+
                      ('code line {}'.format(code_line_id())))
            # print (input_string)
            # example 0,1601,1610,POINT (439963.446 5817857.396),POINT (439802.9 5817830.697)
            inputline_id = input_string.split(',')[0]
            source_id = input_string.split(',')[1]
            target_id = input_string.split(',')[2]
            source_xy = input_string.split(',')[3] # POINT str
            target_xy = input_string.split(',')[4] # POINT str

            # load into shaplely obj and extrac x and y coord. tuple
            source_node = (loads(source_xy).x, loads(source_xy).y)
            target_node = (loads(target_xy).x, loads(target_xy).y)

            # Dict for River Order Columns, place 0, Resets at every route
            if classify_by_RO is True:
                ro_header_dct = defaultdict(list)
                for x in RIVER_ORDER_LST:
                    #  River order Key is the value in the dataset ds_1 or us_1
                    ro_header_dct['down_'+x] = [0.0]
                for x in RIVER_ORDER_LST:
                    ro_header_dct['up_'+x] = [0.0]
            # print(ro_header_dct, 'line 1471')
                
# =============================================================================
# If connected to same edge: This separates routes that will be calculated
# using a the graph vs routes that will use the graph.                 
# =============================================================================
            if new_edges_dct[source_node][11] == new_edges_dct[target_node][11]:
                if source_xy == target_xy: #  ex POINT(0,0)
                    if TESTING_MODE is True:
                        REPORT_LIST.append('Route type: Diagonal (source_xy == target_xy'")."+';'+
                                           ('code line {}'.format(code_line_id()))) 
                    # print (source_xy,'\n', target_xy)
                    # print ('code line {}'.format(code_line_id()))
                    downstream_pct = 0.0
                    upstream_pct = 0.0
                    downstream_length = 0.0
                    upstream_length = 0.0
                    total_length = 0.0
                    route_geometry = loads(source_xy)
                    results_dict[int(inputline_id)] = [(inputline_id)]
                    results_dict[int(inputline_id)].append(source_id)
                    results_dict[int(inputline_id)].append(target_id)
                    results_dict[int(inputline_id)].append(source_xy)
                    results_dict[int(inputline_id)].append(target_xy)
                    results_dict[int(inputline_id)].append(str(round(downstream_pct,6)))
                    results_dict[int(inputline_id)].append(str(round(upstream_pct,6)))
                    results_dict[int(inputline_id)].append(str(round(downstream_length,3)))
                    results_dict[int(inputline_id)].append(str(round(upstream_length,3)))
                    results_dict[int(inputline_id)].append(str(round(total_length,3)))
                    # Add wkt fields
                    if include_wkt is True:
                        #  Add geometry (route_geometry is a shapely obj)
                        results_dict[int(inputline_id)].append(wkt.dumps(route_geometry, trim=False))
                    # Add river order fields
                    if classify_by_RO is True:
                        # # if downstream_length == total_length:
                        # if downstream_pct == 1.0:
                        #         ro_header_dct['down_'+river_order].append(downstream_length)
                        # # if upstream_length == total_length:
                        # elif upstream_pct == 1.0:    
                        #         ro_header_dct['up_'+river_order].append(upstream_length)
                        # else:
                        #     print ('test problem')
                        #     print ('code line {}'.format(code_line_id()))
                        river_order = str(new_edges_dct[source_node][12]) # new v2
                        for x in RIVER_ORDER_LST:
                            results_dict[int(inputline_id)].append(str(round(sum(ro_header_dct['down_'+x]),3)))
                        for x in RIVER_ORDER_LST:
                            results_dict[int(inputline_id)].append(str(round(sum(ro_header_dct['up_'+x]),3)))                                                                
                        # print(results_dict, 'test2')
                        ro_header_dct='This will crash the program if variable\
                            is not reset' # For testing, may delete later.
                        river_order='This will crash the program if variable\
                            is not reset' # For testing, may delete later.                            

                    if len(results_dict) == ROUTE_RESULT_DCT_MAX_SIZE:
                        results_dict = write_route_dct_to_file(
                            outputfile_str, header, fileid,
                            outputfile_write_mode, results_dict, False)  # return empty dict
                        if TESTING_MODE is True:
                            REPORT_LIST.append('Memory dump, write to files.'+';'+
                                               ('code line {}'.format(code_line_id())))

                    routefound += 1
                    continue  # continue will select the next item on the for loop.


                # if new_edges_dct[source_node][0] == 2 and new_edges_dct[target_node][0] == 2:
                elif new_edges_dct[source_node][0] == 2 and new_edges_dct[target_node][0] == 2:
                    # if both lines are split in two
                    if new_edges_dct[source_node][2][1] == new_edges_dct[target_node][2][1]:

                        if TESTING_MODE is True:
                            REPORT_LIST.append('Route type: zero length (Same entry edge node -1'")."+';'+
                                  ('code line {}'.format(code_line_id())))
                        # If the last nodes of both Entry_cost_edge are ==,
                        # then pass and use the Graph lines don't need to be,
                        # resplit.  WKT and 0 lenght work for this.
                        # This could happen when source_xy == target_xy must 
                        # use and "if" clause, not elif. Pass will send to the
                        # graph.
                        pass
                    
                    else:
                        if TESTING_MODE is True:
                            REPORT_LIST.append('Route type: Single edge route, skip graph and resplit and calculate route.'+';'+('code line {}'.format(code_line_id())))
                        
                        resplit_edges_results = resplit_edge(new_edges_dct[source_node], new_edges_dct[target_node], source_id, target_id, include_wkt)
                        # print (resplit_edges_results, 'line {}'.format(code_line_id()))
                        downstream_pct = resplit_edges_results[0]
                        upstream_pct = resplit_edges_results[1]
                        downstream_length = resplit_edges_results[2]
                        upstream_length = resplit_edges_results[3]
                        total_length = resplit_edges_results[4]
                        route_geometry = resplit_edges_results[5] #  shapely.geometry.multilinestring.MultiLineString'
                        # print (new_edges_dct[source_node][12], 'line {}'.format(code_line_id()))

                        results_dict[int(inputline_id)] = [(inputline_id)]
                        results_dict[int(inputline_id)].append(source_id)
                        results_dict[int(inputline_id)].append(target_id)
                        results_dict[int(inputline_id)].append(source_xy)
                        results_dict[int(inputline_id)].append(target_xy)
                        results_dict[int(inputline_id)].append(str(round(downstream_pct,6)))
                        results_dict[int(inputline_id)].append(str(round(upstream_pct,6)))
                        results_dict[int(inputline_id)].append(str(round(downstream_length,3)))
                        results_dict[int(inputline_id)].append(str(round(upstream_length,3)))
                        results_dict[int(inputline_id)].append(str(round(total_length,3)))
                        # Add wkt fields
                        if include_wkt is True:
                            #  Add geometry (route_geometry is a shapely obj)
                            results_dict[int(inputline_id)].append(wkt.dumps(route_geometry, trim=False))
                        # Add river order fields
                        if classify_by_RO is True:
                            river_order = str(new_edges_dct[source_node][12])
                            # if downstream_length == total_length:
                            if downstream_pct == 1.0:
                                    ro_header_dct['down_'+river_order].append(downstream_length)
                            # if upstream_length == total_length:
                            elif upstream_pct == 1.0:    
                                    ro_header_dct['up_'+river_order].append(upstream_length)
                            else:
                                print ('test problem')
                                print ('code line {}'.format(code_line_id()))
                            for x in RIVER_ORDER_LST:
                                results_dict[int(inputline_id)].append(str(round(sum(ro_header_dct['down_'+x]),3)))
                            for x in RIVER_ORDER_LST:
                                results_dict[int(inputline_id)].append(str(round(sum(ro_header_dct['up_'+x]),3)))                                                                
                            # print(results_dict, 'test2')
                            ro_header_dct='This will crash the program if variable\
                                is not reset' # For testing, may delete later.
                            river_order='This will crash the program if variable\
                                is not reset' # For testing, may delete later.                            

                        if len(results_dict) == ROUTE_RESULT_DCT_MAX_SIZE:
                            results_dict = write_route_dct_to_file(
                                outputfile_str, header, fileid,
                                outputfile_write_mode, results_dict, False)  # return empty dict
                            if TESTING_MODE is True:
                                REPORT_LIST.append('Memory dump, write to files.'+';'+
                                                   ('code line {}'.format(code_line_id())))
                        routefound += 1
                        continue  # continue will select the next item on the for loop.
# =============================================================================
#                   Add new nodes to the GRAPH
# as graph is needed. geom1 is the entry cost edge,
# geom2 the first split and geom3 the second split.            
# =============================================================================

            if source_node in dg.nodes():
                remove_source_node = 0
            else:
                if new_edges_dct[source_node][0] == 1:
                    remove_source_node = 1
                    source_new_u1 = new_edges_dct[source_node][2][0]
                    source_new_v1 = new_edges_dct[source_node][2][1]
                    source_geom1 = loads(new_edges_dct[source_node][4])
                    source_len1 = new_edges_dct[source_node][3]
                    if classify_by_RO is True:
                        river_order = new_edges_dct[source_node][12] # new v2
                    dg.add_edge(source_new_u1,source_new_v1)
                    dg.edges[source_new_u1,source_new_v1]['geometry'] = source_geom1
                    dg.edges[source_new_u1,source_new_v1]['infile_length'] = source_len1  # Length must be file variable
                    if classify_by_RO is True:
                        dg.edges[source_new_u1,source_new_v1][str(RO)] = river_order  #  new v2 

                    ug.add_edge(source_new_u1,source_new_v1)
                    ug.edges[source_new_u1,source_new_v1]['geometry'] = source_geom1
                    ug.edges[source_new_u1,source_new_v1]['infile_length'] = source_len1  # Length must be file variable
                    if classify_by_RO is True:
                        ug.edges[source_new_u1,source_new_v1][str(RO)] = river_order  #  new v2 

                elif new_edges_dct[source_node][0] == 2:
                    remove_source_node = 2
                    source_new_u1 = new_edges_dct[source_node][2][0]
                    source_new_v1 = new_edges_dct[source_node][2][1]
                    source_geom1 = loads(new_edges_dct[source_node][4])
                    source_len1 = new_edges_dct[source_node][3]
                    source_new_u2 = new_edges_dct[source_node][5][0]
                    source_new_v2 = new_edges_dct[source_node][5][1]
                    source_geom2 = loads(new_edges_dct[source_node][7])
                    source_len2 = new_edges_dct[source_node][6]
                    source_new_u3 = new_edges_dct[source_node][8][0]
                    source_new_v3 = new_edges_dct[source_node][8][1]
                    source_geom3 = loads(new_edges_dct[source_node][10])
                    source_len3 = new_edges_dct[source_node][9]
                    if classify_by_RO is True:
                        river_order = new_edges_dct[source_node][12] # new v2
                    dg.add_edge(source_new_u1, source_new_v1)
                    dg.edges[source_new_u1, source_new_v1]['geometry']=source_geom1
                    dg.edges[source_new_u1, source_new_v1]['infile_length']=source_len1
                    if classify_by_RO is True:
                        dg.edges[source_new_u1, source_new_v1][str(RO)] = river_order #  new v2 
                    
                    ug.add_edge(source_new_u1, source_new_v1)
                    ug.edges[source_new_u1, source_new_v1]['geometry']=source_geom1
                    ug.edges[source_new_u1, source_new_v1]['infile_length']=source_len1
                    if classify_by_RO is True:
                        ug.edges[source_new_u1, source_new_v1][str(RO)] = river_order #  new v2 
                    # print(ug.edges[source_new_u1, source_new_v1])
                    
                    dg.add_edge(source_new_u2, source_new_v2)
                    dg.edges[source_new_u2, source_new_v2]['geometry']=source_geom2
                    dg.edges[source_new_u2, source_new_v2]['infile_length']=source_len2
                    if classify_by_RO is True:
                        dg.edges[source_new_u2, source_new_v2][str(RO)] = river_order #  new v2 
                    
                    ug.add_edge(source_new_u2, source_new_v2)
                    ug.edges[source_new_u2, source_new_v2]['geometry']=source_geom2
                    ug.edges[source_new_u2, source_new_v2]['infile_length']=source_len2
                    if classify_by_RO is True:
                        ug.edges[source_new_u2, source_new_v2][str(RO)] = river_order #  new v2 

                    dg.add_edge(source_new_u3, source_new_v3)
                    dg.edges[source_new_u3, source_new_v3]['geometry']=source_geom3
                    dg.edges[source_new_u3, source_new_v3]['infile_length']=source_len3
                    if classify_by_RO is True:
                        dg.edges[source_new_u3, source_new_v3][str(RO)] = river_order #  new v2 
                    
                    ug.add_edge(source_new_u3, source_new_v3)
                    ug.edges[source_new_u3, source_new_v3]['geometry']=source_geom3
                    ug.edges[source_new_u3, source_new_v3]['infile_length']=source_len3
                    if classify_by_RO is True:
                        ug.edges[source_new_u3, source_new_v3][str(RO)] = river_order #  new v2 

            # do the same with target nodes
            if target_node in dg.nodes():
                remove_target_node = 0
            else:
                if new_edges_dct[target_node][0] == 1:
                    remove_target_node = 1
                    target_new_u1 = new_edges_dct[target_node][2][0]
                    target_new_v1 = new_edges_dct[target_node][2][1]
                    target_geom1 = loads(new_edges_dct[target_node][4])
                    target_len1 = new_edges_dct[target_node][3]
                    if classify_by_RO is True:
                        river_order = new_edges_dct[target_node][12] # new v2
                    dg.add_edge(target_new_u1,target_new_v1)
                    dg.edges[target_new_u1,target_new_v1]['geometry'] = target_geom1
                    dg.edges[target_new_u1,target_new_v1]['infile_length'] = target_len1  # Length must be file variable
                    if classify_by_RO is True:
                        dg.edges[target_new_u1,target_new_v1][str(RO)] = river_order  #  new v2 
                    
                    ug.add_edge(target_new_u1,target_new_v1)
                    ug.edges[target_new_u1,target_new_v1]['geometry'] = target_geom1
                    ug.edges[target_new_u1,target_new_v1]['infile_length'] = target_len1  # Length must be file variable
                    if classify_by_RO is True:
                        ug.edges[target_new_u1,target_new_v1][str(RO)] = river_order  #  new v2 

                elif new_edges_dct[target_node][0] == 2:
                    remove_target_node = 2
                    target_new_u1 = new_edges_dct[target_node][2][0]
                    target_new_v1 = new_edges_dct[target_node][2][1]
                    target_geom1 = loads(new_edges_dct[target_node][4])
                    target_len1 = new_edges_dct[target_node][3]
                    target_new_u2 = new_edges_dct[target_node][5][0]
                    target_new_v2 = new_edges_dct[target_node][5][1]
                    target_geom2 = loads(new_edges_dct[target_node][7])
                    target_len2 = new_edges_dct[target_node][6]
                    target_new_u3 = new_edges_dct[target_node][8][0]
                    target_new_v3 = new_edges_dct[target_node][8][1]
                    target_geom3 = loads(new_edges_dct[target_node][10])
                    target_len3 = new_edges_dct[target_node][9]

                    if classify_by_RO is True:
                        river_order = new_edges_dct[target_node][12] # new v2
                    dg.add_edge(target_new_u1, target_new_v1)
                    dg.edges[target_new_u1, target_new_v1]['geometry']=target_geom1
                    dg.edges[target_new_u1, target_new_v1]['infile_length']=target_len1
                    if classify_by_RO is True:
                        dg.edges[target_new_u1, target_new_v1][str(RO)] = river_order #  new v2 
                    
                    ug.add_edge(target_new_u1, target_new_v1)
                    ug.edges[target_new_u1, target_new_v1]['geometry']=target_geom1
                    ug.edges[target_new_u1, target_new_v1]['infile_length']=target_len1
                    if classify_by_RO is True:
                        ug.edges[target_new_u1, target_new_v1][str(RO)] = river_order #  new v2 
                    
                    dg.add_edge(target_new_u2, target_new_v2)
                    dg.edges[target_new_u2, target_new_v2]['geometry']=target_geom2
                    dg.edges[target_new_u2, target_new_v2]['infile_length']=target_len2
                    if classify_by_RO is True:
                        dg.edges[target_new_u2, target_new_v2][str(RO)] = river_order #  new v2 
                    
                    ug.add_edge(target_new_u2, target_new_v2)
                    ug.edges[target_new_u2, target_new_v2]['geometry']=target_geom2
                    ug.edges[target_new_u2, target_new_v2]['infile_length']=target_len2
                    if classify_by_RO is True:
                        ug.edges[target_new_u2, target_new_v2][str(RO)] = river_order #  new v2 

                    dg.add_edge(target_new_u3, target_new_v3)
                    dg.edges[target_new_u3, target_new_v3]['geometry']=target_geom3
                    dg.edges[target_new_u3, target_new_v3]['infile_length']=target_len3
                    if classify_by_RO is True:
                        dg.edges[target_new_u3, target_new_v3][str(RO)] = river_order #  new v2 
                    
                    ug.add_edge(target_new_u3, target_new_v3)
                    ug.edges[target_new_u3, target_new_v3]['geometry']=target_geom3
                    ug.edges[target_new_u3, target_new_v3]['infile_length']=target_len3
                    if classify_by_RO is True:
                        ug.edges[target_new_u3, target_new_v3][str(RO)] = river_order #  new v2 


# =============================================================================
# Now that the new edges (including the entry cost edges) are in the graph, try 
# to solve the nearest path using networkX graph.                    
# =============================================================================
            try:
                # Try to find the routes using the graph
                route = nx.dijkstra_path(
                    ug, source_node, target_node, weight='infile_length')

                if TESTING_MODE is True:
                    REPORT_LIST.append('Route type: Try to solve with graph.'+';'+
                          ('code line {}'.format(code_line_id())))
                # ne pas changer weight pour une variable

                route_nodes = []
                # zip route node into paires [1,2,3,4] = [(1,2),(2,3),(3,4)]
                routezip = zip(route, route[1:])
                for i in routezip:
                    route_nodes.append(i)

                total_length = 0.0
                downstream_length = 0.0
                upstream_length = 0.0
                shapely_route = []
                
                for route_edges in route_nodes:
                    u = (route_edges[0])
                    v = (route_edges[1])
                    try:
                        # if node is in Directed graph = downstream_length
                        length = (dg.get_edge_data(u, v)['infile_length'])
                        downstream_length = downstream_length + length
                        total_length = total_length + length
                        
                        if classify_by_RO is True: # new v2
                            river_order = str(dg.get_edge_data(u, v)[str(RO)])
                            ro_header_dct['down_'+river_order].append(length)

                    except:
                        # if node not in the Directed graph = upstream
                        length = (ug.get_edge_data(u, v)['infile_length'])
                        upstream_length = upstream_length + length
                        total_length = total_length + length
                        
                        if classify_by_RO is True: # new v2
                            river_order = str(ug.get_edge_data(u, v)[str(RO)])
                            ro_header_dct['up_'+river_order].append(length)

                    if include_wkt is True:
                        shapely_route.append(ug.get_edge_data(u, v)['geometry'])
                        route_geometry = cascaded_union(shapely_route)

                try:
                    downstream_pct = downstream_length/total_length
                except ZeroDivisionError:
                    downstream_pct = 0.0
                try:
                    upstream_pct = upstream_length/total_length
                except ZeroDivisionError:
                    upstream_pct = 0.0

                results_dict[int(inputline_id)] = [(inputline_id)]
                results_dict[int(inputline_id)].append(source_id)
                results_dict[int(inputline_id)].append(target_id)
                results_dict[int(inputline_id)].append(source_xy)
                results_dict[int(inputline_id)].append(target_xy)
                results_dict[int(inputline_id)].append(str(round(downstream_pct,6)))
                results_dict[int(inputline_id)].append(str(round(upstream_pct,6)))
                results_dict[int(inputline_id)].append(str(round(downstream_length,3)))
                results_dict[int(inputline_id)].append(str(round(upstream_length,3)))
                results_dict[int(inputline_id)].append(str(round(total_length,3)))

                # Add wkt fields
                if include_wkt is True:
                    #  Add geometry (route_geometry is a shapely obj)
                    results_dict[int(inputline_id)].append(wkt.dumps(route_geometry, trim=False))

                # Add river order fields
                if classify_by_RO is True:
                    for x in RIVER_ORDER_LST:
                        results_dict[int(inputline_id)].append(str(round(sum(ro_header_dct['down_'+x]),3)))
                        
                    for x in RIVER_ORDER_LST:
                        results_dict[int(inputline_id)].append(str(round(sum(ro_header_dct['up_'+x]),3)))

                # print ("Route found! total length =",str(total_length))
                # print ("Route found! downstream_length length =",str(downstream_length))
                # print ("Route found! upstream_length length =",str(upstream_length))
                # print('\n')
                route_nodes = []
                routefound += 1
                # Clean up graphs try and except because target nodes may have been
                # remove when source node was removed
                if remove_source_node == 0:
                    pass
                elif remove_source_node == 1:
                    try:
                        dg.remove_node(source_new_u1)
                        ug.remove_node(source_new_u1)
                        remove_source_node = 0
                    except:
                        remove_source_node = 0
                elif remove_source_node == 2:
                    try:
                        dg.remove_node(source_new_u1)
                        ug.remove_node(source_new_u1)
                        dg.remove_node(source_new_v1)
                        ug.remove_node(source_new_v1)
                        remove_source_node == 0
                    except:
                        remove_source_node == 0
                # remove target_node
                if remove_target_node == 0:
                    pass
                elif remove_target_node == 1:
                    try:
                        dg.remove_node(target_new_u1)
                        ug.remove_node(target_new_u1)
                        remove_target_node = 0
                    except:
                        remove_target_node = 0
                elif remove_target_node == 2:
                    try:
                        dg.remove_node(target_new_u1)
                        ug.remove_node(target_new_u1)
                        dg.remove_node(target_new_v1)
                        ug.remove_node(target_new_v1)
                        remove_target_node == 0
                    except:
                        remove_target_node == 0
                # Reset all variables to None
                source_new_u1 = source_new_v1 = source_geom1 = \
                source_len1 = source_new_u2 = source_new_v2 = \
                source_geom2 = source_len2 = source_new_u3 = \
                source_new_v3 = source_geom3 = source_len3 = \
                target_new_u1 = target_new_v1 = target_geom1 = \
                target_len1 = target_new_u2 = target_new_v2 = \
                target_geom2 = target_len2 = target_new_u3 =\
                target_new_v3 = target_geom3 = target_len3 = None

            except nx.NetworkXNoPath as e:
                # if no route is found.
                if TESTING_MODE is True:
                    REPORT_LIST.append('Route type: None found")".'+';'+
                          ('code line {}'.format(code_line_id())))

                results_dict[int(inputline_id)] = [(inputline_id)]
                results_dict[int(inputline_id)].append(source_id)
                results_dict[int(inputline_id)].append(target_id)
                results_dict[int(inputline_id)].append(source_xy)
                results_dict[int(inputline_id)].append(target_xy)
                results_dict[int(inputline_id)].append('-9999')
                results_dict[int(inputline_id)].append('-9999')
                results_dict[int(inputline_id)].append('-9999')
                results_dict[int(inputline_id)].append('-9999')
                results_dict[int(inputline_id)].append('-9999')

                # Add wkt fields
                if include_wkt is True:
                    #  Add geometry (route_geometry is a shapely obj)
                    results_dict[int(inputline_id)].append('-9999')

                # Add river order fields
                if classify_by_RO is True:
                    for x in RIVER_ORDER_LST:
                        results_dict[int(inputline_id)].append('-9999')
                        
                    for x in RIVER_ORDER_LST:
                        results_dict[int(inputline_id)].append('-9999')
                    route = None
                    noroutefound += 1

        if len(results_dict) == ROUTE_RESULT_DCT_MAX_SIZE:
            results_dict = write_route_dct_to_file(
                outputfile_str, header, fileid,
                outputfile_write_mode, results_dict, False)  # return empty dict
            if TESTING_MODE is True:
                REPORT_LIST.append('Memory dump, write to files.'+';'+'code line {}'.format(code_line_id()))

    # final write if dict not empty
    if len(results_dict) == 0:
        pass
    else:
        # print(results_dict, 'code line {}'.format(code_line_id()))
        write_route_dct_to_file(
            outputfile_str, header, fileid, outputfile_write_mode,
            results_dict, True)
        if TESTING_MODE is True:
            REPORT_LIST.append('Final write to files cycle'+';'+ 'code line {}'.format(code_line_id()))

    message = message, 'Routes writen to file', 'found:', routefound, \
        'not found:', noroutefound
    return (message)


def end_of_functions():
    pass

# =============================================================================
#                 END OF FUNCTION DEFINITIONS
# =============================================================================

# =============================================================================
#                 Check 64-bit Python
# =============================================================================
if sys.maxsize > 2**32:
    pass
else:
    sys.exit(
        'Your Python interpreter is 32-bit.  Please use 64-bit')

# =============================================================================
#                         GLOBAL VARIABLES
# =============================================================================


#Create output directories
create_directory(OUTPUT_ROUTE_SUBDIRECTORY)
create_directory(OUTPUT_GRAPH_SUBDIRECTORY)
create_directory(OUTPUT_TIE_OUTSIDE_NODE_SUBDIRECTORY)
create_directory(PYTHO_LIST_DICT_VARIABLES_SUBDIRECTORY)

# =============================================================================
# ******************************Make Graphs
# Graph are simple graphs with no parallel edges.  Parallel edges are split
# in two and added to the graph. Old parallel edges are add to the DataFrame.
# =============================================================================
print( '            *************', BASIN, '*************','\n')
GPD_DF_EDGES_NETWORK = read_shape_file_to_gpd_df(
    INPUT_NETWORK_FILE_SHP, 'LineString')

if CLASSIFY_BY_RIVER_ORDER is True:
    RIVER_ORDER_LST = parse_river_order(
        INPUT_NETWORK_FILE_SHP)

G = make_graph_from_gpd_df(GPD_DF_EDGES_NETWORK)
DG = G[0]
UG = G[1]
GPD_DF_EDGES_NETWORK_NO_PARALLELS_EDGES = G[2]
print('DG and UG graphs done.')

# =============================================================================
#                      Find node that are not in Graphs
# =============================================================================
# read source_target point file




GPD_DF_NODES_FOR_ROUTES = read_shape_file_to_gpd_df(
    INPUT_SOURCE_TARGET_POINTS, 'Point')
print('GPD_DF_NODES_FOR_ROUTES: done','\n')
print('Calculating entry costs to the graph. This may take time...','\n')

if USE_SOURCE_AND_TARGET_FILES is False:
    NEW_EDGES_DCT = tie_outside_node(
            GPD_DF_NODES_FOR_ROUTES, GPD_DF_EDGES_NETWORK_NO_PARALLELS_EDGES,
            DG)
    GPD_DF_TARGET_NODES_FOR_ROUTES = ''

elif USE_SOURCE_AND_TARGET_FILES is True:
    GPD_DF_TARGET_NODES_FOR_ROUTES = read_shape_file_to_gpd_df(
        INPUT_TARGET_POINTS, 'Point')
    SOURCE_TARGET_CONTAT = pd.concat(
        [GPD_DF_NODES_FOR_ROUTES,GPD_DF_TARGET_NODES_FOR_ROUTES])
    NEW_EDGES_DCT = tie_outside_node(
        SOURCE_TARGET_CONTAT, GPD_DF_EDGES_NETWORK_NO_PARALLELS_EDGES, DG)
    print('GPD_DF_NODES_FOR_ROUTES: done','\n')
    print('Calculating entry costs to the graph. This may take time...','\n')
    



# =============================================================================
#                 Pickle python variable
# =============================================================================
# Pickle Us pickle for trouble shooting dictionnary.
# Can be use as a backup of variable
# These lines can be used to make a New Edges from a pickled file.
# pickle_to_file(NEW_EDGES_DCT, 'NEW_EDGES_DCT.pickle')
# NEW_EDGES_DCT_PICKLE_STR = os.path.join(PYTHO_LIST_DICT_VARIABLES_SUBDIRECTORY, BASIN+'_new_edges_dct.pickle')
# NEW_EDGES_DCT_PICKLE_F = open(NEW_EDGES_DCT_PICKLE_STR,'rb')
# NEW_EDGES_DCT_PICKLE = pickle.load(NEW_EDGES_DCT_PICKLE_F)
# NEW_EDGES_DCT_PICKLE_F.close()
# print (len(NEW_EDGES_DCT_PICKLE))

if TESTING_MODE is True:
    REPORT_LIST=[]
    input('Entering single thread testing mode.  Press Enter to continue...')
    check_network_graph(DG,UG)
    write_graph_to_csv(DG, GRAPH_EDGE_FILE_OUTPUT, GRAPH_NODES_FILE_OUTPUT)
    write_new_edges_dct_to_csv(NEW_EDGES_DCT)
    upper_matrix_route_list = distance_matrix_route(GPD_DF_NODES_FOR_ROUTES, GPD_DF_TARGET_NODES_FOR_ROUTES)

    
    # Filter Upper_matrix_route_list
    # small_list = []
    # for lines in upper_matrix_route_list:
    #     if int(lines.split(',')[0]) < 5:
    #         small_list.append(lines)
    # print (len(small_list),'small list len')
    # upper_matrix_route_list = small_list
    
    
    pickle_to_file(NEW_EDGES_DCT, 'NEW_EDGES_DCT.pickle')
    pickle_to_file(upper_matrix_route_list, 'upper_matrix_route_list.pickle')
    print(calculate_routes(upper_matrix_route_list))
    write_testing_report(REPORT_LIST)
    sys.exit()

# =============================================================================
#                          MAIN
# =============================================================================

if __name__ == '__main__':
    print('********** _main_ started **********' )
    print('Checking directed and undirected Graphs','\n')
    check_network_graph(DG,UG)  # for trouble shooting network

    print('Writing graphs nodes and edges to csv files.')
    write_graph_to_csv(DG, GRAPH_EDGE_FILE_OUTPUT, GRAPH_NODES_FILE_OUTPUT)

    print('Saving sources, targets nodes and networks entry costs to csv.')
    write_new_edges_dct_to_csv(NEW_EDGES_DCT)

    print('Calculating source and target routes jobs. Writing to csv file.')
    upper_matrix_route_list = distance_matrix_route(
        GPD_DF_NODES_FOR_ROUTES, GPD_DF_TARGET_NODES_FOR_ROUTES)

    print('Pickling variables to csv files.')
    pickle_to_file(NEW_EDGES_DCT, 'NEW_EDGES_DCT.pickle')
    pickle_to_file(upper_matrix_route_list, 'upper_matrix_route_list.pickle')

# =============================================================================
#              Place a condition on list for testing
# =============================================================================

    # small_list = []
    # for lines in upper_matrix_route_list:
    #     if int(lines.split(',')[0]) < 58:
    #         small_list.append(lines)
    # upper_matrix_route_list = small_list
    # print('Upper_matrix_route_list has been reduced to ',len(upper_matrix_route_list),'querries')
    # print('**************')


# *********  Make a nested list of n THREADS * n with n N_POINTS.**************

    print('Multiprocessing started','\n','************')
    P = Pool(THREADS)
    GROUPERLST = []
    for x in grouper(upper_matrix_route_list, N_POINTS):
        GROUPERLST.append(x)
    print ('Grouping jobs done.','\n')
    print('Calculating routes with', THREADS, 'threads.')
    for results in P.imap_unordered(calculate_routes, GROUPERLST):
        print(results)
        print('\n')
        GROUPERLST = []
    P.close()
    P.join()
    STOP_TIME = time.time()
    TOTAL_TIME = (STOP_TIME-START_TIME)/60
    print('done:', TOTAL_TIME, 'minutes')

