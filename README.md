# upstream_downstream_shortests_path_dijkstra

Vector based shortest path analysis in GIS is well established for road networks.  Even though these network algorithms can be applied to river layers, they do not generally consider the direction of flow.  This is a Python 3.7 program developed specifically for river networks.  It implements an “all-pairs” weighted Dijkstra shortest path algorithm to return the route geometry, the total distance between each source and target node and the total upstream and downstream distances for each shortest path.  The program is intended to be used in a multiprocessor environment but can be used with a single multithreaded CPU or a High-Performance Computing Cluster (HPCC). In a case study, it was used to calculate ~135 million routes in a river basin containing 4413 river segments in under 88 hours using a desktop system with a 6 core CPU (i7-6800K @ 3.4 GHz) and 64 GB of RAM.

This program calculates the upper half of a distance matrix (site A to site B).  To calculate the lower half, the diagonal or to stitch all the results into one big files, please see the ShortestPath_matrix_builder.py (https://gitlab.com/njacadieux/shortestpath_matrix_builder)

Paper containing usage and algorithm description can be found at https://www.mdpi.com/2306-5729/5/1/8

Citing this software: Cadieux, N.; Kalacska, M.; Coomes, O.T.; Tanaka, M.; Takasaki, Y. A Python Algorithm for Shortest-Path River Network Distance Calculations Considering River Flow Direction. Data 2020, 5, 8. 


New: v2.0
- Checks for 64-bit Python interpreter.
- Can now calculates distances by river order ('RO').
- Can now calculate the full matrix instead of only upper half. This will take more than twice the time but useful for small runs.
- New testing mode for single thread testing with verbose output in output file.
- Can now work with a source node and target node file (instead of a single combined source and target file).  This effectively implements a one (sources) to all (destinations) Dijkstra.
- Distance_maxtrix_route() function replaces upper_distance_maxtrix_route().
- Graph now contains all the variables from the input network file. This will facilitate future development.

See "NEW FEATURES, FIXES AND KNOW ISSUES" in source code for more information.
